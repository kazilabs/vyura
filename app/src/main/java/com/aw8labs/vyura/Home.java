package com.aw8labs.vyura;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class Home extends AppCompatActivity {

    private static String TAG = "Home";

    private TextView username;
    private TextView points;
    private ImageView profileImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Reference to view objects
        username = (TextView) findViewById(R.id.username);
        profileImage = (ImageView) findViewById(R.id.profile_image);

        // Capture data from the intent
        String name = getIntent().getStringExtra("name");
        String photoUrl = getIntent().getStringExtra("photoUrl");
        Log.i(TAG,"name: "+name);

        // set the username
        username.setText(name);

        // Load the image using picasso
        Picasso.with(this).load(photoUrl).into(profileImage);

    }
}
