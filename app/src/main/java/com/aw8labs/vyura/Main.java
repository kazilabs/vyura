package com.aw8labs.vyura;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;

public class Main extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final int RC_SIGN_IN = 9001;
    private static final String TAG = "Main";
    private static final String Log_In="logged_in";

    GoogleApiClient mGoogleApiClient;
    private Boolean loggedIn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Button Listeners
        findViewById(R.id.sign_in_button).setOnClickListener(this);

        // Configure sign-in to request the user's ID, email address and basic profile.
        // ID and basic profile are included in DEFAULT_SIGN_IN
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // GoogleApiClient with access to the Google Sign-In API and the options
        // specified by gso
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();
    }

    @Override
    public void onClick(View v) {
        // Handle button clicks
        // Use a switch statement to determine which buttons are clicked
        switch (v.getId()){

            // When the sign-in button is clicked sign the user in
            case R.id.sign_in_button:
                signIn();
                break;
        }

    }

    private void signIn(){

        // Starting the intent prompts a user to select a google account to sign with.
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Results returned from launching the Intent from GoogleSignInApi.getSignIntent
        if(requestCode == RC_SIGN_IN){

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result){
        Log.d(TAG, "handleSignResult:" + result.isSuccess());
        if(result.isSuccess()){
            // Sign in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();


            //Start home activity
            Intent intent = new Intent(this,Home.class);

            //send user information to the home activity
            intent.putExtra("name",acct.getDisplayName());
            intent.putExtra("email",acct.getEmail());

            Log.i(TAG, "" + acct.getDisplayName());
            Log.i(TAG, ""+acct.getPhotoUrl());

            // Only send the photo url if it exists
            if(acct.getPhotoUrl() != null){
                intent.putExtra("photoUrl",acct.getPhotoUrl().toString());
            }

            startActivity(intent);
            this.finish();

        }else{
            // Signed out, show authenticated UI.

        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        // An unresolvable  error has occurred and Google APIs
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    @Override
    protected void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
//            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
//                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }
}
